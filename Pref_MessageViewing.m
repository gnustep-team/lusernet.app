/*
 *
 * Copyright 2002 Alexander Malmberg <alexander@malmberg.org>
 *           2012 The Free Software Foundation
 *
 * Authors: Alexander Malmberg <alexander@malmberg.org>
 *          Riccardo Mottola <rm@gnu.org>
 *
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSUserDefaults.h>
#import <Foundation/NSBundle.h>
#import <Foundation/NSDictionary.h>

#import <AppKit/NSButton.h>
#import <AppKit/NSFontManager.h>
#import <AppKit/NSPopUpButton.h>
#import <AppKit/NSTextField.h>
#import <GNUstepGUI/GSVbox.h>
#import <GNUstepGUI/GSHbox.h>
#import <AppKit/NSFont.h>

#import <Pantomime/CWCharset.h>

#import "Pref_MessageViewing.h"


#define ColorMessagesKey @"ColorMessages"

#define MessageFont1Key @"MessageFont1"
#define MessageFont1SizeKey @"MessageFont1Size"

#define MessageFont2Key @"MessageFont2"
#define MessageFont2SizeKey @"MessageFont2Size"

#define IntelligentScrollKey @"IntelligentScroll"

#define AutoDownloadSizeLimitKey @"AutoDownloadSizeLimit"


static NSUserDefaults *sd;

@implementation Pref_MessageViewing

+(void) initialize
{
	if (!sd)
		sd=[NSUserDefaults standardUserDefaults];
}

+(int) autoDownloadSizeLimit
{
	if ([sd objectForKey: AutoDownloadSizeLimitKey])
		return [sd integerForKey: AutoDownloadSizeLimitKey];
	else
		return 64*1024;
}

+(BOOL) colorMessages
{
	if (![sd objectForKey: ColorMessagesKey] ||
	    [sd boolForKey: ColorMessagesKey])
		return YES;
	else
		return NO;
}

+(BOOL) intelligentScroll
{
	if (![sd objectForKey: IntelligentScrollKey] ||
	    [sd boolForKey: IntelligentScrollKey])
		return YES;
	else
		return NO;
}

+(NSFont *) font1
{
	NSString *s;
	double size;
	NSFont *f;

	size=[sd floatForKey: MessageFont1SizeKey];
	f=nil;
	if ((s=[sd stringForKey: MessageFont1Key]))
	{
		f=[NSFont fontWithName: s  size: size];
	}
	if (!f)
		f=[NSFont userFontOfSize: size];
	return f;
}

+(NSFont *) font2
{
	NSString *s;
	double size;
	NSFont *f;

	size=[sd floatForKey: MessageFont2SizeKey];
	f=nil;
	if ((s=[sd stringForKey: MessageFont2Key]))
	{
		f=[NSFont fontWithName: s  size: size];
	}
	if (!f)
		f=[NSFont userFixedPitchFontOfSize: size];
	return f;
}

-(void) save
{
	NSUserDefaults *sd=[NSUserDefaults standardUserDefaults];
	BOOL b;

	if (!top) return;

	b=[b_ColorMessages state]?YES:NO;
	[sd setBool: b forKey: ColorMessagesKey];

	b=[b_IntelligentScroll state]?YES:NO;
	[sd setBool: b forKey: IntelligentScrollKey];

	[sd setInteger: [f_AutoDownloadSizeLimit intValue] forKey: AutoDownloadSizeLimitKey];

	[sd setFloat: [[f_font1 font] pointSize] forKey: MessageFont1SizeKey];
	[sd setObject: [[f_font1 font] fontName] forKey: MessageFont1Key];

	[sd setFloat: [[f_font2 font] pointSize] forKey: MessageFont2SizeKey];
	[sd setObject: [[f_font2 font] fontName] forKey: MessageFont2Key];
}

-(void) revert
{
	NSFont *f;

	if ([Pref_MessageViewing colorMessages])
		[b_ColorMessages setState: 1];
	else
		[b_ColorMessages setState: 0];

	if ([Pref_MessageViewing intelligentScroll])
		[b_IntelligentScroll setState: 1];
	else
		[b_IntelligentScroll setState: 0];

	[f_AutoDownloadSizeLimit setIntValue: [Pref_MessageViewing autoDownloadSizeLimit]];

	f=[Pref_MessageViewing font1];
	[f_font1 setStringValue: [NSString stringWithFormat: @"%@ %0.1f",[f fontName],[f pointSize]]];
	[f_font1 setFont: f];

	f=[Pref_MessageViewing font2];
	[f_font2 setStringValue: [NSString stringWithFormat: @"%@ %0.1f",[f fontName],[f pointSize]]];
	[f_font2 setFont: f];
}


-(NSString *) name
{
	return _(@"Message viewing");
}

-(void) setupButton: (NSButton *)b
{
	[b setTitle: _(@"Message\nviewing")];
	[b sizeToFit];
}

-(void) willHide
{
}

-(NSView *) willShow
{
	if (!top)
	{
		top=[[GSVbox alloc] init];
		[top setDefaultMinYMargin: 4];

		{
			NSButton *b;

			b_ColorMessages=b=[[NSButton alloc] init];
			[b setTitle: _(@"Color lines in messages based on quoting depth.")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[top addView: b enablingYResizing: NO];
			DESTROY(b);

			b_IntelligentScroll=b=[[NSButton alloc] init];
			[b setTitle: _(@"Scroll intelligently (ie. skip quoted sections and signatures).")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[top addView: b enablingYResizing: NO];
			DESTROY(b);
		}

		[top addSeparator];

		{
			NSTextField *f;
			NSButton *b;
			NSPopUpButton *pop;
			GSHbox *hb;

			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Automatically download messages smaller than:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_AutoDownloadSizeLimit=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable];
			[f sizeToFit];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);


			[top addSeparator];


			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Message font #2:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_font2=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
			[f setEditable: NO];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			b=[[NSButton alloc] init];
			[b setTitle: _(@"Pick font...")];
			[b setTarget: self];
			[b setAction: @selector(pickFont2:)];
			[b sizeToFit];
			[hb addView: b  enablingXResizing: NO];
			DESTROY(b);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);


			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Message font #1:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_font1=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable|NSViewHeightSizable];
			[f setEditable: NO];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			b=[[NSButton alloc] init];
			[b setTitle: _(@"Pick font...")];
			[b setTarget: self];
			[b setAction: @selector(pickFont1:)];
			[b sizeToFit];
			[hb addView: b  enablingXResizing: NO];
			DESTROY(b);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);

			[top addSeparator];

			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Default encoding ")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			pop = [[NSPopUpButton alloc] init];
			[pop setPullsDown:NO];
			NSDictionary *charsets = [CWCharset allCharsets];
			NSEnumerator *en = [[charsets allValues] objectEnumerator];
			NSString *encoding;
			while((encoding = [en nextObject]))
			  {
			    [pop addItemWithTitle:encoding];
			  }
			[pop selectItemWithTitle:
			       [charsets 
				 objectForKey:
				   [sd objectForKey:@"DefaultEncoding"]]];
			[pop synchronizeTitleAndSelectedItem];
			[pop setTarget: self];
			[pop setAction: @selector(changeEncoding:)];
			[pop sizeToFit];
			[hb addView: pop  enablingXResizing: NO];
			DESTROY(pop);


			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);

		}

		[self revert];
	}
	return top;
}

-(void) dealloc
{
	DESTROY(top);
	[super dealloc];
}


-(void) _pickFont
{
	NSFontManager *fm=[NSFontManager sharedFontManager];
	[fm setSelectedFont: [f_cur font] isMultiple: NO];
	[fm orderFrontFontPanel: self];
}

-(void) pickFont1: (id)sender
{
	f_cur=f_font1;
	[self _pickFont];
}

-(void) pickFont2: (id)sender
{
	f_cur=f_font2;
	[self _pickFont];
}

-(void) changeFont: (id)sender
{
	NSFont *f;

	if (!f_cur) return;
	f=[sender convertFont: [f_cur font]];
	if (!f) return;

	[f_cur setStringValue: [NSString stringWithFormat: @"%@ %0.1f",[f fontName],[f pointSize]]];
	[f_cur setFont: f];
}

-(void) changeEncoding:(id)sender
{
  NSDictionary *charsets = [CWCharset allCharsets];
  sd=[NSUserDefaults standardUserDefaults];

  [sender selectItemWithTitle:[sender titleOfSelectedItem]];
  [sender synchronizeTitleAndSelectedItem];
  
  [sd setObject:
	[[charsets allKeysForObject:[sender titleOfSelectedItem]] objectAtIndex:0] 
      forKey:@"DefaultEncoding"];
}

+(NSString *) defaultEncoding
{
  return [sd stringForKey:@"DefaultEncoding"];
}

@end

