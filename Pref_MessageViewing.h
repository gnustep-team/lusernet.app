/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef Pref_MessageView_h
#define Pref_MessageView_h

#include "PrefBox.h"

@class GSVbox,NSTextField,NSFont;


@interface Pref_MessageViewing : NSObject <PrefBox>
{
/* only top is directly retained */
	GSVbox *top;

	NSButton *b_ColorMessages,*b_IntelligentScroll;
	NSTextField *f_AutoDownloadSizeLimit;
	NSTextField *f_font1,*f_font2,*f_cur;
}

+(int) autoDownloadSizeLimit;

+(BOOL) colorMessages;
+(BOOL) intelligentScroll;

+(NSFont *) font1;
+(NSFont *) font2;
+(NSString *) defaultEncoding;

@end

#endif

