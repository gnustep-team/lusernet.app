/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef ComposeWindowController_h
#define ComposeWindowController_h

@class NSString,NSDictionary;
@class CWMessage;
@class NSTextView,NSTextField,NSButton;

@interface ComposeWindowController : NSWindowController
{
	int state;

	NSDictionary *msg_headers;

	NSTextView *text;
	NSTextField *tf_from,*tf_newsgroups,*tf_subject;
	NSButton *b_send;
}

/* designated initializer */
- initWithHeaders: (NSDictionary *)headers  content: (NSString *)content;

- initWithHeaders: (NSDictionary *)headers  quoteContent: (NSString *)content;
- initWithHeaders: (NSDictionary *)headers;
- initWithFollowupToMessage: (CWMessage *)msg;

-(void) postedArticle: (BOOL)success;

@end

#endif

