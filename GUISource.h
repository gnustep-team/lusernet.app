/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef GUISource_h
#define GUISource_h

/*
Things a MsgDB_Source can implement so it can be configured easily in the
preferences panel.
*/

@protocol GUISource
-(NSString *) sourceName;
-(NSString *) sourceType;

-(void) displayPropertiesWindow;
@end

#endif

