/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef Pref_Sources_h
#define Pref_Sources_h

@class MsgDB,GSHbox,NSTableView,NSTableColumn;

@interface Pref_Sources : NSObject <PrefBox>
{
	MsgDB *mdb;

	NSArray *sources;

	GSHbox *top;
	NSTableView *source_list;
	NSTableColumn *c_num,*c_name,*c_type;
}

- initWithMsgDB: (MsgDB *)mdb;
@end

#endif

