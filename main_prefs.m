/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#include <Foundation/NSObject.h>

#include "main.h"

#include "PrefBox.h"

#include "Pref_Sources.h"
#include "Pref_MessageViewing.h"
#include "Pref_ReadAhead.h"
#include "Pref_Posting.h"

#include "PreferencesWindowController.h"


@implementation AppDelegate (prefs)
-(void) openPreferences: (id)sender
{
	if (!preferences_window)
	{
		NSObject<PrefBox> *pb;
		preferences_window=[[PreferencesWindowController alloc] init];

		pb=[[Pref_Sources alloc] initWithMsgDB: mdb];
		[preferences_window addPrefBox: pb];
		DESTROY(pb);

		pb=[[Pref_MessageViewing alloc] init];
		[preferences_window addPrefBox: pb];
		DESTROY(pb);

		pb=[[Pref_ReadAhead alloc] init];
		[preferences_window addPrefBox: pb];
		DESTROY(pb);

		pb=[[Pref_Posting alloc] init];
		[preferences_window addPrefBox: pb];
		DESTROY(pb);
	}
	[preferences_window showWindow: self];
}
@end

