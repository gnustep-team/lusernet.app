/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
          2012 Riccardo Mottola <rm@gnu.org>
*/

#include <Foundation/NSObject.h>
#include <Foundation/NSString.h>
#include <Foundation/NSUserDefaults.h>
#import <Foundation/NSBundle.h>
#include <GNUstepGUI/GSVbox.h>
#include <GNUstepGUI/GSHbox.h>
#include <AppKit/NSButton.h>
#include <AppKit/NSTextField.h>
#include <AppKit/NSPopUpButton.h>
#include <AppKit/NSOpenPanel.h>

#include "Pref_Posting.h"


#define PostingNameKey @"PostingName"
#define FromAddressKey @"FromAddress"

#define PostQuotedPrintableKey @"PostQuotedPrintable"


#define SignatureTypeKey @"SignatureType"
#define SignatureValueKey @"SignatureValue"

#define SIGNATURE_LITERAL 0
#define SIGNATURE_FILE    1
#define SIGNATURE_PIPE    2
#define SIGNATURE_MAX     2


static NSUserDefaults *sd;

@implementation Pref_Posting

+(void) initialize
{
	if (!sd)
		sd=[NSUserDefaults standardUserDefaults];
}


+(NSString *) postingSignature
{
	int type=[sd integerForKey: SignatureTypeKey];
	NSString *s;

	s=[sd stringForKey: SignatureValueKey];
	if (!s || ![s length])
		return nil;

	switch (type)
	{
	default:
	case SIGNATURE_LITERAL:
		return s;

	case SIGNATURE_FILE:
	{
		NSString *sig=[NSString stringWithContentsOfFile: s];
		if (sig)
			return sig;

		return [NSString stringWithFormat: _(@"Unable to read signature from '%@'.\n"),s];
	}

	case SIGNATURE_PIPE:
	{
		FILE *p=popen([s cString],"r");
		char buf[256];
		int l;
		NSMutableString *sig=[[NSMutableString alloc] init];

		if (!p)
			return [NSString stringWithFormat: _(@"Unable to get signature by running '%@'.\n"),s];

		while ((l=fread(buf,1,sizeof(buf)-1,p))>0)
		{
			buf[l]=0;
			[sig appendString: [NSString stringWithCString: buf]];
		}

		pclose(p);

		return AUTORELEASE(sig);
	}

	}
}


+(NSString *) fromAddress
{
	return [sd stringForKey: FromAddressKey];
}

+(NSString *) postingName
{
	return [sd stringForKey: PostingNameKey];
}

+(BOOL) postQuotedPrintable
{
	if (![sd objectForKey: PostQuotedPrintableKey] ||
	    [sd boolForKey: PostQuotedPrintableKey])
		return YES;
	else
		return NO;
}

-(void) save
{
	NSUserDefaults *sd=[NSUserDefaults standardUserDefaults];
	BOOL b;

	if (!top) return;

	b=[b_PostQuotedPrintable state]?YES:NO;
	[sd setBool: b forKey: PostQuotedPrintableKey];

	[sd setObject: [f_FromAddress stringValue] forKey: FromAddressKey];
	[sd setObject: [f_PostingName stringValue] forKey: PostingNameKey];

	[sd setInteger: [b_SignatureType indexOfSelectedItem] forKey: SignatureTypeKey];
	[sd setObject: [f_SignatureValue stringValue] forKey: SignatureValueKey];
}

-(void) revert
{
	if ([Pref_Posting postQuotedPrintable])
		[b_PostQuotedPrintable setState: 1];
	else;
		[b_PostQuotedPrintable setState: 0];

	[f_FromAddress setStringValue: [Pref_Posting fromAddress]];
	[f_PostingName setStringValue: [Pref_Posting postingName]];

	{
		int idx=[sd integerForKey: SignatureTypeKey];
		if (idx<0 || idx>SIGNATURE_MAX)
			idx=0;
		[f_SignatureValue setStringValue: [sd stringForKey: SignatureValueKey]];
		[b_SignatureType selectItemAtIndex: idx];
	}
}


-(NSString *) name
{
	return _(@"Posting");
}

-(void) setupButton: (NSButton *)b
{
	[b setTitle: _(@"Posting")];
	[b sizeToFit];
}

-(void) willHide
{
}


-(void) _signatureBrowse: (id)sender
{
	NSOpenPanel *op=[NSOpenPanel openPanel];
	int result;

	[op setAllowsMultipleSelection: NO];
	[op setCanChooseFiles: YES];
	[op setCanChooseDirectories: NO];
	result=[op runModalForTypes: nil];
	if (result==NSOKButton && [op filename])
	{
		[f_SignatureValue setStringValue: [op filename]];
	}
}


-(NSView *) willShow
{
	if (!top)
	{
		top=[[GSVbox alloc] init];
		[top setDefaultMinYMargin: 4];

		{
			NSButton *b;

			b_PostQuotedPrintable=b=[[NSButton alloc] init];
			[b setTitle: _(@"Post articles as quoted-printable")];
			[b setButtonType: NSSwitchButton];
			[b sizeToFit];
			[top addView: b enablingYResizing: NO];
			DESTROY(b);
		}

		[top addSeparator];

		{
			NSTextField *f;
			NSPopUpButton *pb;
			NSButton *b;
			GSHbox *hb;

			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Source:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_SignatureValue=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable];
			[f sizeToFit];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			b=[[NSButton alloc] init];
			[b setTitle: _(@"Browse...")];
			[b setTarget: self];
			[b setAction: @selector(_signatureBrowse:)];
			[b sizeToFit];
			[hb addView: b  enablingXResizing: NO];
			DESTROY(b);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);


			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Signature type:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			b_SignatureType=pb=[[NSPopUpButton alloc] init];
			[pb setAutoenablesItems: NO];
			[pb addItemWithTitle: _(@"Literal")];
			[pb addItemWithTitle: _(@"Contents of file")];
			[pb addItemWithTitle: _(@"Output from program")];
			[pb sizeToFit];
//			[pb setAutoresizingMask: NSViewWidthSizable];
			[hb addView: pb  enablingXResizing: YES];
			DESTROY(pb);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);
		}

		[top addSeparator];

		{
			NSTextField *f;
			GSHbox *hb;

			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"From-address:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_FromAddress=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable];
			[f sizeToFit];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);


			hb=[[GSHbox alloc] init];
			[hb setDefaultMinXMargin: 4];
			[hb setAutoresizingMask: NSViewWidthSizable];

			f=[[NSTextField alloc] init];
			[f setStringValue: _(@"Name:")];
			[f setEditable: NO];
			[f setDrawsBackground: NO];
			[f setBordered: NO];
			[f setBezeled: NO];
			[f setSelectable: NO];
			[f sizeToFit];
			[f setAutoresizingMask: 0];
			[hb addView: f  enablingXResizing: NO];
			DESTROY(f);

			f_PostingName=f=[[NSTextField alloc] init];
			[f setAutoresizingMask: NSViewWidthSizable];
			[f sizeToFit];
			[hb addView: f  enablingXResizing: YES];
			DESTROY(f);

			[top addView: hb enablingYResizing: NO];
			DESTROY(hb);
		}

		[self revert];
	}
	return top;
}

-(void) dealloc
{
	DESTROY(top);
	[super dealloc];
}

@end

