/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef FolderWindowController_h
#define FolderWindowController_h

#include <AppKit/NSWindowController.h>

@class MsgDB;
@class NSString,NSTableView,NSTableColumn,NSScrollView;
@class MessageViewController,FolderThreader;

/* TODO: declare <Msg_Folder> instead */
#include "MsgDB.h"

@interface FolderWindowController : NSWindowController
{
	NSString *folder_name;
	MsgDB *mdb;

	NSObject<Msg_Folder> *folder;

	FolderThreader *ft;

	MessageViewController *mv;

	NSTableView *list;
	NSTableColumn *c_subject,*c_date,*c_from;
	NSScrollView *listsv;

	int sort_mode;
	NSMutableArray *sorted_msgs;
	msg_id_t cur_selection;

	/* this size must match the stuff in the readahead category */
	msg_id_t cur_read_ahead[4];
}
- initWithMsgDB: (MsgDB *)m  folder: (NSString *)fname;

@end

#endif

