/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#ifndef NNTPSource_h
#define NNTPSource_h

@class NNTPServer;
@class MsgDB;
@class NSMutableDictionary;

struct nntpsource_group_s
{
	int idx;
	int last_num;
};


@interface NNTPSource : NSObject <Msg_Source>
{
	NNTPServer *server;
	MsgDB *mdb;
	msg_id_t mmid;

	int num_groups;
	struct nntpsource_group_s *groups;

	char *cur_host,*cur_sport;

	NSMutableDictionary *request_map;
}

-(void) postArticle: (unsigned char *)data  length: (int)length
	sender: (NSObject *)sender;

@end

#endif

