/*
copyright 2002 Alexander Malmberg <alexander@malmberg.org>
*/

#include <Foundation/NSObject.h>

#include "KeyWindow.h"


@implementation KeyWindow

-(void) keyDown: (NSEvent *)e
{
	if ([self delegate])
		if ([[self delegate] respondsToSelector: @selector(keyDown:inWindow:)])
			if ([[self delegate] keyDown: e  inWindow: self])
				return;
	[super keyDown: e];
}

@end

